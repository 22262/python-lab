# lines = f1.readlines()
# for i in range(0, len(lines)):
#   if i%2 == 0:
#     f2.write(lines[i])

# f1 = open("file1.txt", "r")
# f2 = open("file2.txt", "w")
# count = 1  
# for line in f1:
#   if(count %2 != 0):
#       f2.write(line)
#   count = count + 1

with open("file1.txt", "r") as f1:
  with open("file2.txt", "w") as f2:
    count = 1
    for line in f1:
      if count %2 != 0:
        f2.write(line)
      count += 1