import csv

# with open("name.csv", "r") as file1:
#   csv_reader = csv.reader(file1);

#   with open("new_name.csv", "w") as file2:
#     csv_writer = csv.writer(file2, delimiter=",")
#     for line in csv_reader:
#       csv_writer.writerow(line)


with open("name.csv", "r") as file1:
  csv_reader = csv.DictReader(file1)
  
  with open("new_name.csv", "w") as file2:
    fieldnames = ['first_name', 'last_name', 'email']
    csv_writer = csv.DictWriter(file2, fieldnames = fieldnames, delimiter="\t")

    csv_writer.writeheader()

    for line in csv_reader:
      csv_writer.writerow(line)