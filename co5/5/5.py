import csv

fieldnames = ['name', 'age']
with open("details.csv", "w") as file:
  d = [{'name': 'arjun', 'age': 18}, {'name': 'amal', 'age': 20}, {'name': 'aravind', 'age': 50}, {'name': 'rais', 'age': 40}]
  csv_writer = csv.DictWriter(file, fieldnames= fieldnames, delimiter=",")
  csv_writer.writeheader()
  csv_writer.writerows(d)